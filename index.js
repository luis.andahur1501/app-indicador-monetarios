/*
Crear una nueva app que me permita hacer seguimiento de los valores de indicadores monetarios.
Menu
1. Actualizar datos(Escribe nuevo archivo).
2.- Promedio ultimos 5 archivos
3.- Minimo y maximo ultimos 5 archivos.
4.- Salir

Observaciones
- Utilice la API mi indicador (mindicador.cl)
- Haga uso de promesas
- La persistencia de la aplicacion debe ser en archivos JSON.
- Divida la aplicacion en los siguientes modulos :
    DataAPI
    FilesAPI
 */
const fs = require ('fs');
const fetch = require('node-fetch');
const readline = require('readline');

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


function opcionMenu(opcion){
    switch(opcion){
        case '1': console.log('Opcion 1');
        fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then((datos)=>{fs.writeFile('indicadores.json',JSON.stringify(datos),(err)=>{console.error(err)}); 
        }).catch(console.error);
        break;
        case '2': getFile('../DesafioNode/indicadores.json').then(JSON.parse).then((datos)=>{obtenerPromedio(datos)}).catch(console.error);
        
        break;
        case '3': 
        
        break;
        case '4': process.exit(0);
        break;
        default: 
        console.log('Opcion no encontrada');
        break;
    }
//leerOpcion().then(  (opcion)=>{opcionMenu(opcion);} ).catch(console.error);
}

const status = response =>{
    if(response.status >= 200 && response.status < 300){
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
};
const obtenerJson = response => {
    return response.json();
};
const getFile = fileName => {
    return new Promise((resolve,reject)=>{
        fs.readFile(fileName,(err,data)=>{
            if(err){
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}
function obtenerPromedio(json){
    //console.log(json['uf'].valor);
    //Hacer foreach del json, despues pasar el elemento a JSON.parse(elem) y hacer for each al elemento
    var info="PROMEDIOS \n";
    for (var elem in json){
        //console.log(elem);
        if(elem!='version' && elem!='autor' && elem!='fecha'){
            info+=elem+": "+json[elem].valor+"\n";
        }
    }
    console.log(json);
    //leerOpcion().then(  (opcion)=>{opcionMenu(opcion);} ).catch(console.error);
}
// const writeFile = fileName => {
//     return new Promise((resolve,reject)=>{
//         fs.writeFile(fileName, (err,data)=>{
//             if(err){
//                 reject(err);
//                 return;
//             }
//             resolve(data);
//         });
//     });
// };

var leerOpcion = ()=>{
    return new Promise((resolve,reject)=>{
        console.log('Menu');
        console.log('1.- Actualizar Datos');
        console.log('2.- Ultimos 5 promedios');
        console.log('3.- Minimo y maximo ultimos 5 archivos');
        console.log('4.- Salir');    
        lector.question('Escriba su opcion: ', opcion =>{
            resolve(opcion);
        });
    });
}


/*fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then((datos)=>{fs.writeFile('indicadores.json',JSON.stringify(datos),(err)=>{console.error(err)}); 
}).catch(console.error);*/

leerOpcion().then((opcion)=>{opcionMenu(opcion);}).catch(console.error);